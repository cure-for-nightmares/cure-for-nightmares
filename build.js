"use strict";

/*
 * === Table of Contents ===
 *
 * Setup
 * Scan files
 * Render files
 */


/* =====================================
	Setup
===================================== */

// Load and configure modules.
var moment = require( "moment" );
var fs = require( "fs" );
var path = require( "path" );
var cson = require( "cson" );
var url = require( "url" );
var child_process = require( "child_process" );

var pug = require( "pug" );
// This objct is a container for options that need to get passed to pug, which
// we will define later... See Pug API reference at <https://pugjs.org/api/>.
// It also includes objects containing site-wide metadata, and helper functions
// used in the templates.
var pugOptions = {
	/* ========== Site-wide metadata ========== */

	site: {
		title: "Cure For Nightmares",
		tagline: "We are alive whether we like it or not — so why not?",
		domain: "https://curefornightmares.com",
	},

	// When including and extending templates, absolute paths are based on the
	// current working directory (which *should* be the project root, when
	// compiling pages).
	basedir: process.env.PWD,

	// Which media hosting provider to use. Normally media gets served faster
	// when we host it ourselves (value set to "self"), but in case we want to
	// save bandwidth, we can change this string to "archive", to switch to
	// using the Internet Archive instead.
	defaultHost: "archive",

	niccolo: {
		email: "niccolo@curefornightmares.com"
	},

	andrew: {
		email: "andrew@tosk.in",
		web: "https://andrew.tosk.in",
	},

	hikaru: {
		name: "hikaru starr",
		web: {
			main: "https://plus.google.com/+hikarustarr/",
			secondary: "https://instagram.com/hikarustarr/",
		},
		rel: "author friend",
	},
};

/* String generator functions, also added to pugOptions */

var coauthorsList = function ( story ) {
	if ( story.coauthors ) {
		return "Niccolo Florence, " + story.coauthors;
	}
	else {
		return "Niccolo Florence";
	}
};

// For story and picture posts, we use a somewhat more subdued header
// style, so the site header doesn't distract from the main content.
var headerType = function ( page ) {
	if ( page.date ) {
		return "header-story-posts";
	}
	else {
		return "header-archive-meta";
	}
};

// Generate a URL for the breadcrumb page links.
var breadcrumbLink = function ( page ) {
	if ( page.category === "short stories" ) {
		return "/alphabetically#short-stories";
	}
	else if ( page.category === "photography" ) {
		return "/alphabetically#photography";
	}
	else if ( page.category === "sketches" ) {
		return "/alphabetically#sketches";
	}
	else if ( page.category === "old" ) {
		return "/alphabetically#earliest-attempts";
	}
};

pugOptions.coauthorsList = coauthorsList;
pugOptions.headerType = headerType;
pugOptions.breadcrumbLink = breadcrumbLink;

// NodeJS's `url.format` function will also be useful for Pug formatting.
pugOptions.urlFormat = url.format;


var stylus = require( "stylus" );



/* =====================================
	Scan files
===================================== */

// List of directories that need to be created before we can put files in them.
var publicFolders = [];

// Collections are subsets of things in `/pages/` directory, sorted by some
// category.
var collections = {
	// Pages that list everything in posts, so you can browse the full archive.
	archiveLists: [],
	// All stories, sorted chronologically.
	chronologically: [],
	// Error pages.
	errors: [],
	// All the "about" pages ("meta" in WordPress lingo).
	meta: [],
	// Old stories, which I don't quite want to delete, but aren't very good.
	old: [],
	// All story and sketch/photo pages, which have dates. All the main content.
	posts: {
		// All the photography posts.
		photography: [],
		// All the sketch posts.
		sketches: [],
		// All the short stories.
		shortStories: [],
	},
};

// List of stylesheets to process, when we render Stylus into CSS.
var stylesheets = [];

// List of static files, which will get directly copied at build time.
var staticFiles = [];

// Output path copies `pages/*`, `static/*`, and `styles/` itself to
// `public/`, changes extension for rendered files.
var getOutputPath = function ( inputPath ) {
	// Replace first directory with `public/`.
	return inputPath.replace( /^(pages|static)/, "public" )
		.replace( /^styles/, "public/styles" )
		// Convert feed page's file extension.
		.replace( /feed\.pug$/, "feed.atom" )
		// Replace .pug file extension.
		.replace( /\.pug$/, ".html" )
		// Replace .stylus file extension.
		.replace( /\.stylus$/, ".css" );
};

// Recursively scan files and add to lists for later processing.
var scanFiles = function ( startPath ) {
	// Files in the current directory.
	var directory = fs.readdirSync( startPath );

	if ( startPath === "styles" ) {
		publicFolders.push( "public/styles" );
	}

	for ( var file of directory ) {
		// Relative path to the current file.
		var currentPath = path.join( startPath, file );
		var outputPath = getOutputPath( currentPath );

		// Recurse into directories, and add to list of public folders that need
		// to be created.
		if ( fs.statSync( currentPath ).isDirectory()) {
			publicFolders.push( outputPath );
			scanFiles( currentPath );
		}
		// Add Pug files to the appropriate collections.
		else if ( path.extname( currentPath ) === ".pug" ) {
			// Scan inside the current file to parse CSON metadata.
			var fileContents = fs.readFileSync( currentPath, "utf8" );
			var csonString = fileContents.match( /---([^]+?)---/ )[ 1 ];
			var pageProperties = cson.parse( csonString );

			// The page's paths will be usefull too.
			pageProperties.sourcePath = currentPath;
			pageProperties.outputPath = outputPath;
			if ( pageProperties.outputPath === "public/index.html" ) {
				pageProperties.url = "/";
			}
			else if ( pageProperties.outputPath === "public/feed.atom" ) {
				pageProperties.url = "/feed.atom";
			}
			else {
				pageProperties.url = outputPath.replace( /^public(.*)\.html$/, "$1" );
			}

			if ( pageProperties.date ) {
				collections.chronologically.push( pageProperties );
			}

			// Which sub-category in collections does the current file belong to?
			if ( currentPath.includes( "pages/short-stories" )) {
				pageProperties.category = "short stories";
				collections.posts.shortStories.push( pageProperties );
			}
			else if ( currentPath.includes( "pages/photography" )) {
				pageProperties.category = "photography";
				collections.posts.photography.push( pageProperties );
			}
			else if ( currentPath.includes( "pages/sketches" )) {
				pageProperties.category = "sketches";
				collections.posts.sketches.push( pageProperties );
			}
			else if ( currentPath.includes( "pages/about" )) {
				pageProperties.category = "meta";
				collections.meta.push( pageProperties );
			}
			else if ( currentPath.includes( "pages/old" )) {
				pageProperties.category = "old";
				collections.old.push( pageProperties );
			}
			else if ( currentPath.includes( "pages/errors" )) {
				pageProperties.category = "errors";
				collections.errors.push( pageProperties );
			}
			// The last few pages remaining should be archive lists.
			else {
				pageProperties.category = "archive lists";
				collections.archiveLists.push( pageProperties );
			}
		}
		// Add Stylus files to list of stylesheets to render.
		else if ( path.extname( currentPath ) === ".stylus" ) {
			stylesheets.push({
				sourcePath: currentPath,
				outputPath: outputPath,
			});
		}
		// Add everything else to the list of static files.
		else {
			staticFiles.push({
				sourcePath: currentPath,
				outputPath: outputPath,
			});
		}
	}
};

var sortCollections = function () {
	// Sort posts alphabetically, by category.
	for ( var category in collections.posts ) {
		// For any two elements a and b in the list, compare the url properties,
		// since those will already be lowercase and omit 'a' and 'the' from the
		// beginning.
		collections.posts[ category ].sort( function ( a, b ) {
			if ( a.url < b.url ) {
				return -1;
			}
			else if ( a.url > b.url ) {
				return 1;
			}
			else {
				return 0;
			}
		});
	}
	// Sort chronological collection too, with most recent first.
	collections.chronologically.sort( function ( a, b ) {
		if ( a.date > b.date ) {
			return -1;
		}
		else if ( a.date < b.date ) {
			return 1;
		}
		else {
			return 0;
		}
	});
};

// Okay, scan pages.
scanFiles( "pages" );
scanFiles( "styles" );
scanFiles( "static" );

sortCollections();

// Pug needs access to page collections and MomentJS for templates to find
// next/previous page in the archive, and to format dates.
pugOptions.pretty = "\t";
pugOptions.collections = collections;
pugOptions.moment = moment;

// Let's review the collections we've built.
console.log( "\nThe collections object:\n" );
console.log( collections );
for ( var category in collections.posts ) {
	console.log( "\n" + category );
	console.log( collections.posts[ category ]);
}



/* =====================================
	Render files
===================================== */

// Recursively delete files in a directory, as cleanup before building.
var deleteAllFiles = function ( startPath ) {
	// Files in the current directory.
	var directory = fs.readdirSync( startPath );

	for ( var file of directory ) {
		// Relative path to the current file.
		var currentPath = path.join( startPath, file );

		// Recurse into folders, and then delete folders when empty.
		if ( fs.statSync( currentPath ).isDirectory()) {
			deleteAllFiles( currentPath );
			fs.rmdirSync( currentPath );
		}
		// Just delete all regular files.
		else {
			fs.unlinkSync( currentPath );
		}
	}
};

// Okay, we're finally ready.
// Clean out the public directory before building.
deleteAllFiles( "public" );

// Create empty subdirectories of `public/` so we can put stuff in them.
for ( var subdirectory of publicFolders ) {
	fs.mkdirSync( subdirectory, 0o755 );
}

// Render Pug files.
console.log( "\n\nRENDERING PUG FILES.\n" );
for ( var i in collections ) {
	// Skip the `posts` collection, since these are duplicated in
	// `chronologically`.
	if ( i !== "posts" ) {
		for ( var j = 0; j < collections[ i ].length; j++ ) {
			// Also tell Pug which is the current page, and for chronological
			// sort, also the previous, and next pages.
			pugOptions.currentPage = collections[ i ][ j ];
			if ( i === "chronologically" ) {
				if ( collections[ i ][ j + 1 ]) {
					pugOptions.previousPage = collections[ i ][ j + 1 ];
				}
				else {
					pugOptions.previousPage = false;
				}
				if ( j > 0 ) {
					pugOptions.nextPage = collections[ i ][ j - 1 ];
				}
				else {
					pugOptions.nextPage = false;
				}
			}
			console.log( collections[ i ][ j ].title );
			console.log( "  " + collections[ i ][ j ].sourcePath + " → " + collections[ i ][ j ].outputPath );
			console.log( "  index: " + i + ": " + j );
			var renderedHTML = pug.renderFile( collections[ i ][ j ].sourcePath, pugOptions );
			fs.writeFileSync( collections[ i ][ j ].outputPath, renderedHTML, "utf8" );

			// And compress the files after rendering.
			console.log( "  Pre-compressing file for Nginx..." );
			child_process.exec( "gzip --best --keep --name " + collections[ i ][ j ].outputPath );
		}
	}
}

// Render Stylus files.
for ( var file of stylesheets ) {
	var stylusString = fs.readFileSync( file.sourcePath, "utf8" );
	var cssString = stylus.render( stylusString, { filename: file.outputPath });
	fs.writeFileSync( file.outputPath, cssString, "utf8" );

	// Compress the stylesheets.
	child_process.exec( "gzip --best --keep --name " + file.outputPath );
}

// Directly copy static files.
for ( var file of staticFiles ) {
	var fileData = fs.readFileSync( file.sourcePath );
	fs.writeFileSync( file.outputPath, fileData );

	// The regex should be based on the server's configuration for file types to
	// be compressed -- but manually add HTML.
	var compressibleFiletypes = /\.(0p|1|1p|2|3|3p|4|5|6|7|8|9|ascii|atom|bash|c|cf|coffee|conf|cpp|crt|cs|css|csv|d|dart|default|der|desktop|erl|ex|exs|fish|go|h|hrl|hs|htm|html|hx|hxml|ical|icalendar|ics|ifb|ini|jade|jl|js|json|jsonld|kt|kts|latex|lhs|lisp|litcoffee|log|ls|ltx|lua|markdown|md|mdcomic|mdfilm|mdown|mml|otf|pem|php|pl|pm|pug|py|rb|rkt|rlib|rs|rss|sc|scala|scm|scrbl|service|sh|shtml|spec|ss|swift|tex|troff|ts|ttf|txt|vala|vapi|vcard|vcf|wml|xml|zsh)$/;
	// Compress the compressible files.
	var extension = path.extname( file.outputPath );
	if ( extension.match( compressibleFiletypes )) {
		child_process.exec( "gzip --best --keep --name " + file.outputPath );
	}
}
