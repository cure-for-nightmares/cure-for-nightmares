---
title: "By Default"
date: "2012-06-25t12:00"
layout: "story"
series: "old"
license: "cc-share-alike"
blurb: "My very first comic, made by combining some old photos with a cathartic letter I wrote when someone asked how I’d been doing. The relationship between the text and the images is kind of abstract, but I’d hoped the images helped carry the tone of escapist fantasy which resolves at the end of the story."

# Building the complete URL for all the media.
host:
	toskin: "https://media.tosk.in/albums/andrew/fumetti/short-stories/by-default/"
	archive: "https://ia902505.us.archive.org/4/items/By-Default--by-Niccolo-Florence/"
base_name: "by-default--"
mediatype: ".jpg"

# Comic script goes in the image alt text.
comic_script:
	part-1: "Panel 1: Title and Byline\n\n	By Default: Tales of Customer Service\n	by Niccolo Florence\n	curefornightmares.com\n\nPanel 2:\n\n	I don’t know why. Something about these jobs…\n\n	I can’t explain it to anyone who hasn’t worked in customer\n	service before, and for those who have, no explanation is\n	needed. They merely look at the scars from where they\n	accidentally stapled themselves (over and over) during a\n	moment of barely contained emotion — and they understand.\n\n	So maybe I won’t explain why.\n\nPanel 3:\n\n	But rage has become my default mood at CVS. The day’s shift\n	gets better when Norma comes in to buy cat food and talk\n	about her grandkids, or Walter drops off a box of cupcakes\n	for the photo department to divvy among the rest of us.\n\n	And there are days like today where I keep remembering lines\n	from a Brian Regan standup comedy show and I draw stares as\n	I burst out laughing at random moments.\n\n	Those days are the exceptions.\n\nPanel 4:\n\n	Yesterday, I was at the front counter by myself.\n\n	The supervisor had gone off to deal with another customer,\n	and my only checking backups were on break. So of course\n	this is when I heard the sound of marching feet in the\n	distance. I had a Jurassic Park sort of moment where I froze\n	and watched a cup of water quiver on the dashboard, and\n	suddenly 80 people filed up to the counter at once,\n\n	*snarling after my tasty human flesh.*\n\n	I frantically ran around, trying to get people out the door\n	as fast as I could — can I help you with anything else have a\n	nice day *ding* — all the while keeping track of the\n	paperwork that builds up along the way. But this wasn’t\n	enough.\n\nPanel 5:\n\n	“Could you call someone else to do this,” one man said, his\n	exasperated tone implying, “because an armless monkey could\n	do a better job.”"
	part-2: "Panel 6:\n\n	“No,” I said, without looking up.\n\n	“No one at all?” He groaned.\n\n	So I stopped. “You know what? You’re right, that’s a\n	brilliant idea: Call someone else to help check so I’m not\n	battling the hordes alone—why didn’t I think of that!”\n\n	“There’s no need to get smart,” he snapped.\n\n	“Well *you* certainly seem to think so!”\n\nPanel 7:\n\n	I said “You don’t seem to be doing anything. Why don’t *you*\n	check, then?”\n\n	And I hefted the receipt printer, which was heavy and had\n	pointed corners — I lifted it high over my head…\n\nPanel 8:\n\n	Then everything went black for a second.\n\n	I thought maybe the power had gone out in the store. But\n	when I woke up the line of customers was gone, and there was\n	blood all over my hands.\n\n	…Okay, so I’m kidding. Seriously, though: Inspiration\n	comes at the weirdest times. Shortly after this mad rush of\n	grumpy shoppers, a song came to me.\n\nPanel 9:\n\n	An entire song, a whole symphony popped out of my head like\n	a cork, and fizzy harmonies, contrapuntal foam gushed all\n	over the counter. I don’t know if I’ll be able to write it\n	all down before it goes flat.\n\n	And that’s pretty much been my life these past few months.\n\nEND."

# Where users can download source files.
source:
	flickr: "https://www.flickr.com/photos/andrewtoskin/sets/72157645505948575/"
	archive: "https://archive.org/details/By-Default--by-Niccolo-Florence"
---
