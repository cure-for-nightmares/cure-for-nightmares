---
title: "Mother Badger"
coauthors: "hikaru starr"
date: "2013-10-10t23:32"
layout: "story"
series: "short stories"
license: "cc-noncommercial"
blurb: "I spent more time living with my father, but I remember the time spent living with my mother more vividly…"

# Building the complete URL for every image.
host:
	toskin: "https://media.tosk.in/albums/andrew/fumetti/short-stories/mother-badger/"
	archive: "https://ia801402.us.archive.org/13/items/Mother-Badger--by-Niccolo-Florence/"
base_name: "mother-badger--"
mediatype: ".jpg"

# Comic script for posts with text embedded in the imagery.
comic_script:
	part-1: "Panel 1: We’re standing under the boughs of a giant Christmas tree, the green\nneedles seeming to glow from the thousands of lights. Underneath stands a\nlife-size porcelain nativity scene.\n\n	@Title and Byline\n		> Mother Badger: Growing Up In a Sett  \n		words by Niccolo Florence\n		photography by hikaru starr\n		http://curefornightmares.com\n\nPanel 2: We see a playground slide… except it seems to be the only playground\nequipment around. There are some trees and some patchy grass, but nothing else.\nThe slide’s paint is losing its color, and the slide itself was designed with\ntoo many unfriendly straight lines.\n\n	caption: It was a time of innocence.\n\n	caption: …Saying that probably brings to mind images of romping in the\n	grass, collecting bugs, rolling down hills, blowing dandelion seeds, which is\n	all wrong.\n\nPanel 3: Pink and white tulips.\n\n	caption: Not that those things didn’t happen — they did — but my memory of\n	those days isn’t exactly boiling over with scenes of chasing dragonflies.\n\nPanel 4: A Japanese paper lantern, split horizontally through the middle. It\ndangles on the hinge of its last paper ribs. White light pours out onto the\nnearby wall.\n\n	caption: It was a time of innocence, as in I thought it was perfectly normal\n	for my mother to reach for a knife when she was angry.\n\n	caption: She was less the protective mother hen and more the crazy mother\n	badger: We were strange cubs in her sett, and she would crack open our skulls\n	and eat our brains whenever she noticed us skulking about.\n\n	caption: She literally bit me once.\n\nPanel 5: A lurid close shot of fish at the market. Their identical faces are\nall silver-blue, but their bodies are glistening pink.\n\n	caption: She threw my brother into a bath tub of scalding water when he\n	interrupted her soap opera. I once asked her what was for dinner, and she\n	locked me out of the house for 2 days."
	part-2: "Panel 6: Glass Christmas ornaments, mostly in reds and golds, lie all over the\nfloor.\n\n	caption: I actually laughed and felt sorry for him when one of my friends\n	mentioned how his parents actually made dinner and listened to him\n\n	caption: …at least, during commercial breaks. Didn’t they have the decency\n	to teach him to fend for himself?\n\n	caption: It didn’t occur to me until after I had to explain for the 3rd time\n	how I fell down an elevator shaft that maybe my friends weren’t the ones\n	missing out on something. Maybe it was me.\n\n	caption: Maybe the tooth fairy usually gave kids money for their teeth instead\n	of a punch in the mouth. Maybe Santa Claus was real after all, but Mother\n	scared him away with her shotgun that one noisy Christmas Eve.\n\nPanel 7: Another Christmas tree, this one with a strong blue and purple color\nscheme.\n\n	caption: …Or maybe he was a random drunk. I don’t know.\n\n	caption: I did wake up late that night, though, first to a loud clumsy\n	thumping through the ceiling, then someone singing and chuckling loudly to\n	himself. “…is coming to town. Ho! Ho ho!”\n\n	caption: I staggered sleepily down the hall to find out what was going on.\n	Even the tree lights were off, so it was dark; I could barely make out the\n	shape of a man moving about my living room, a shadow puppet sliding on top of\n	shadow puppets — but I could smell eggnog, with just a hint of manure, like\n	he’d been out drinking with reindeer all night.\n\nPanel 8: Out on the porch, behind another Christmas tree, hang some kind of\nhandkerchief decorations. In the blur of the photo’s bokeh, it almost looks\nlike a row of ghosts hanging from the eaves.\n\n	caption: I never saw Mother rush past me, she just appeared. Burrowed up out\n	of the ground, mad as a wolverine. Thunder cracked, the whole house jumped in\n	fright, a spark lit up the room like a single colored frame in a longer\n	animation reel of grays.\n\n	caption: The buckshot only smashed the window, I think, but the strange man\n	took the opportunity to dive through this new exit.\n\n	caption: Like the shotgun, Mother spoke only once: “Don’t.” And she shuffled\n	on back to her room."
	part-3: "Panel 9: The white outside wall of a shed. Rough rusty red holes have been\ngouged into the wall, with rust streaking down the sides like blood.\n\nPanel 10: Sproutlings emerge from the cracks in a tree stump. They are already\ngrowing little pink buds.\n\n	caption: Looking back, it makes me wonder how my sister Natalie could be so\n	well traveled, while I’ve spent most of my life so far afraid of adventures…\n\nPanel 11: More trees decorated with Christmas lights. These aren’t pine or fir,\nthough. It’s a little hard to tell from the photo: The right third of the image\nis filled with the texture of tree bark and winding green electrical wires, and\nthe rest of the photo is lost to bright orange lights in bokeh.\n\n	caption: Mother was a minefield. If I wanted anything from her, I had to\n	approach slowly, wave my metal detector in front of me, and gingerly test each\n	step.\n\n	caption: *Sometimes the mines moved.*\n\nEND."

# Where users can download source files.
source:
	flickr: "https://www.flickr.com/photos/andrewtoskin/sets/72157646970670838/"
	archive: "https://secure.flickr.com/photos/andrewtoskin/sets/72157646970670838/"
---

I hadn't intended this to be a Christmas story per se, but I come from a messed
up family, and the decorations are already starting up to show up in department
stores. So why not --- I need to be ready for "Black Friday." Here you go.

Check out hikaru's
<a href="@collaborators.hikaru.web.main" rel="@collaborators.hikaru.rel">other</a>
<a href="@collaborators.hikaru.web.secondary" rel="@collaborators.hikaru.rel">photographic</a>
work!
