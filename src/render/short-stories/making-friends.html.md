---
title: "Making Friends"
date: "2015-04-13t16:50"
layout: "story"
series: "short stories"
license: "cc-share-alike"
blurb: "Without the perspective that comes from experience, children are truly able to live in the moment: Even now, you may still remember being trapped in a moment that seemed to last forever."

# Building the complete URL for every image.
host:
	toskin: "https://media.tosk.in/albums/andrew/fumetti/short-stories/making-friends/"
	archive: "https://ia601508.us.archive.org/6/items/Making-Friends--by-Niccolo-Florence/"
base_name: "making-friends--"
mediatype: ".jpg"

# Comic script goes in the image alt text.
comic_script:
	# Note the hyphen in panel/part labels; in the story loop, this is converted to part of the file URL.
	full: "Panel 1: The art here should look like papercuts: We start out with a boy, the\nshy new kid in town, standing alone in the long, narrow frame, looking\nuncertainly out at something off-Panel. He has a sheep face and actual chicken legs.\n\n	TITLE AND BYLINE\n		Making Friends\n		by Niccolo Florence\n		curefornightmares.com\n\nPanel 2: The new kid shyly approaches another boy at a sandbox. This other boy\nlooks up from the sand castle he's building, which stands between the two.\n\n	@New Kid\n		> My family just moved out here, so I don't know anybody, and\n		I'm feeling kind of scared and lonely. Can we be friends?\n\n	@Other Boy\n		> Sure. We don't know each other yet, so I can't guarantee\n		we'll be BFFs or whatever, but I'm willing to give you a chance.\n		And I certainly don't mind showing you around.\n\nPanel 3: The boys hug.\n\n	@New Kid\n		> Thank you. I really appreciate this.\n\n	@Other Boy\n		> Sure, sure. I think we’ve all been there.\n\nPanel 4: It turns out that all the previous Panels are all inside a thought\ncloud for the first boy, who sits curled up in a ball, looking pitiful\nunder a slide. Other kids use the slide, seemingly unaware of him.\n\nEND."

# Where users can download source files.
source:
	flickr: "https://www.flickr.com/photos/andrewtoskin/sets/72157651958133405"
	archive: "https://archive.org/details/Making-Friends--by-Niccolo-Florence"

# Attributions (for stories that borrow CC works, rather than collaborating with someone)
attributions:
	"Design Shard":
		url: "http://www.designshard.com/"
		works:
			"Paper and Cardboard Textures": "http://www.designshard.com/freebies/18-free-paper-cardboard-textures/"
			"Burnt Paper Textures": "http://www.designshard.com/freebies/19-free-hi-res-burnt-paper-textures/"
	"Franz Jeitz, Fudgegraphics":
		url: "http://www.fudgegraphics.com/"
		works:
			"Old Grunge Paper Textures": "http://www.fudgegraphics.com/2009/06/free-hi-res-old-grunge-paper-textures/"
	"Caleb Kimbrough, Lost and Taken":
		url: "http://lostandtaken.com/"
		works:
			"Vintage Wallpaper": "http://lostandtaken.com/blog/2009/5/12/5-free-vintage-wallpaper-textures.html"
			"Vintage Wallpaper II": "http://lostandtaken.com/blog/2009/6/3/vintage-damask-textures-part-ii.html"
---
