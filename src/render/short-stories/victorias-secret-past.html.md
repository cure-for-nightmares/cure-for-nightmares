---
title: "Victoria’s Secret Past"
date: "2013-08-18t11:26"
layout: "story"
series: "short stories"
license: "cc-share-alike"
blurb: "I’ve been studying their lingerie catalogs for a while now, and I’m almost certain there’s a message hidden between the lace and feathers." # remember: manually convert to smart punctuation here.

# Building the complete URL for every image.
host:
	toskin: "https://media.tosk.in/albums/andrew/fumetti/short-stories/victorias-secret-past/"
	archive: "https://ia601402.us.archive.org/19/items/Victorias-Secret-Past--by-Niccolo-Florence/"
base_name: "victorias-secret-past--"
mediatype: ".jpg"

# Comic script goes in the image alt text.
comic_script:
	full: "Panel 1 of 1: Victoria’s Secret supermodel Miranda Kerr kneels in a pink background, wearing a purple bra and panties, a ruby necklace, and the classic Angel wings. She looks off to the side. She has a knife in one hand.\n\nCaptions: Every time I see a Victoria’s Secret catalog, I feel like I’m reading a mystery novel. Did she kill him in the study with a garter belt? What’s the secret? What *is* sexy?? The suspense is killing me!"

# Where users can download source files.
source:
	flickr: "https://www.flickr.com/photos/andrewtoskin/sets/72157646025986013/"
	archive: "https://archive.org/details/Victorias-Secret-Past--by-Niccolo-Florence"
---
