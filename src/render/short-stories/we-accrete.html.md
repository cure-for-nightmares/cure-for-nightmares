---
title: "We Accrete"
date: "2015-09-14t13:45"
layout: "story"
series: "short stories"
license: "cc-share-alike"
blurb: "Binge-reading science articles makes me feel romantic."

# Building the complete URL for every image.
host:
	toskin: "https://media.tosk.in/albums/andrew/fumetti/short-stories/we-accrete/"
	archive: "https://ia601506.us.archive.org/16/items/we-accrete--by-niccolo-florence/"
base_name: "we-accrete--"
mediatype: ".jpg"

# Comic script for posts with text embedded in the imagery.
comic_script:
	part-1: "Panel 1: image \"A Galactic Spectacle\": It’s actually a picture of the collision\nbetween two separate galaxies, but it looks now like a single mass of lights\nand color. A \"golden spiral,\" resembling some snail shells.\n\n	Title and Byline\n		﻿We Accrete\n		by Niccolo Florence\n		curefornightmares.com\n\nPanel 2: image \"a dusty planet-forming disk, similar to the one that vanished around the\nstar called TYC 8241 2652.\"\n\n	caption: You and I\n		circle each other, merely\n		one cluster of dust and rock and debris\n		near a hungry new star.\n		We swing around on gravity’s merry-go-round\n		and lean our heads back in the solar wind.\n\nPanel 3: image \"Planetary Smash-Up\": Two planetoids collide with a terrific burst of\nfiery light and hot fragments.\n\n	caption: You and I,\n		the closer we get, the closer we get,\n		the tighter we become,\n		until we become one\n		mass of warming carbon and iron and nickel\n		in a shroud of hydrogen and helium.\n\nPanel 4: image \"Artist’s impression of a gas giant planet forming in the disc around\nthe young star HD 100546\": The young planet glows red-hot in the fog of the\nnewly-forming star system.\n\n	caption: You and I,\n		our core melts red-hot.\n		Gradually the mantle separates\n		into stratified layers by density.\n		Molten minerals boil beneath the surface\n		until our crust splits\n		and releases the magma in our pit."
	part-2: "Panel 5: image \"Terraformed Mars\": We see a planet sort of like Earth, with swirling\nwhite clouds, red and green continents, and lots of blue water.\n\n	caption: You and I\n		get wet — steam pours from our pores\n		and condenses in the atmosphere we exhaled.\n		We release again and again\n		over the millennia,\n		belching our satisfaction as if from a delicious meal.\n\nPanel 6: image \"Pequeño Planeta Crossroads\": a photo of a city park, bent so it looks\nsort of like a little planet, with a circle of grass and cement in the\ncenter, bushes and trees sprouting all around the edges. Tall city buildings\njut up from the western side.\n\n	caption: You and I\n		turn green — life appears.\n		It shows up out of nowhere.\n		I frown when I notice the algae on our face,\n		but then it evolves,\n		and things wriggle and swim in our oceans\n		and some crawl onto the beaches.\n		They wheeze from the struggle, vestigial gills dilated,\n		sand coating their slimy bellies.\n		I try to throw some lava on the nasty things,\n		but this only seems to ignite some Cambrian explosion\n		of new species.\n		It is unnerving, and disgusting, and vile,\n		and a little cute.\n\nPanel 7: image \"Black Marble\": We have a view of the Earth at night. The golden\nglitter of city lights sparkles on Asia, Africa, and all over Europe.\n\n	caption: You and I\n		are excited.\n		We never knew we had been lonely\n		until we had children to keep us company.\n\nEND."

# Where users can download source files.
source:
	flickr: "https://www.flickr.com/photos/andrewtoskin/sets/72157658221497708"
	archive: "https://archive.org/details/we-accrete--by-niccolo-florence"

# Attributions (for stories that borrow CC works, rather than collaborating with someone)
attributions:
	"NASA/JPL-Caltech":
		url: "http://www.jpl.nasa.gov/"
		works:
			"a dusty planet-forming disk, similar to the one that vanished around the star called TYC 8241 2652": "https://www.nasa.gov/mission_pages/WISE/news/wise20120705.html"
			"Planetary Smash-Up": "https://www.nasa.gov/multimedia/imagegallery/image_feature_1454.html"
	"NASA Goddard Space Flight Center":
		url: "https://www.nasa.gov/centers/goddard"
		works:
			"A Galactic Spectacle": "https://www.flickr.com/photos/gsfc/4862916839/"
	"European Southern Observatory / L. Calçada":
		url: "http://www.eso.org/"
		works:
			"Artist’s impression of a gas giant planet forming in the disc around the young star HD 100546": "https://www.flickr.com/photos/esoastronomy/8556091519/"
	"Ittiz":
		url: "https://commons.wikimedia.org/wiki/User:Ittiz~commonswiki"
		works:
			"Terraformed Mars": "https://commons.wikimedia.org/wiki/File:TerraformedMars.jpg"
	"Nestor Ferraro":
		url: "https://www.flickr.com/photos/nestorferraro/"
		works:
			"Pequeño Planeta Crossroads": "https://www.flickr.com/photos/nestorferraro/14795270424/"
	"NASA Earth Observatory / Robert Simmon and Chris Elvidge":
		url: "http://earthobservatory.nasa.gov/"
		works:
			"Black Marble - Europe, Africa, and the Middle East": "https://www.flickr.com/photos/gsfc/8247962102/in/album-72157632175125121/"
---

Evelyn and I recently rewatched both Cosmos series --- as you can probably tell --
and I've been getting lost in Wikipedia, and *Shiva Christ*, everything is so
beautiful and fascinating, I feel like my head is on fire.
