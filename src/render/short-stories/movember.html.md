---
title: "Movember"
date: "2015-12-02t13:15"
layout: "story"
series: "short stories"
license: "cc-share-alike"
blurb: "It’s sorta funny to me that some of the most successful viral “awareness-raising” campaigns in recent years have come to overshadow the charity or the cause they’re promoting. The No-Shave November thing is such a victim of its own success that there are men who stop shaving for the month, but don’t know anything about why someone started the trend."

# Building the complete URL for every image.
host:
	toskin: "https://media.tosk.in/albums/andrew/fumetti/short-stories/movember/"
	archive: "https://ia801505.us.archive.org/26/items/Movember--by-Niccolo-Florence/"
base_name: "movember--"
mediatype: ".jpg"

# Comic script for posts with text embedded in the imagery.
comic_script:
	part-1: "Panel 1: We start with a self-portrait, drawn the way I drew people when I was 4\nyears old, with the arms and legs growing directly out of my head.\n\n	caption: When I was a kid I figured I’d probably end up growing a beard, not\n	because I really wanted one, but because I’d be too lazy to shave.\n\nPanel 2: Me and Girlfriend in high school, having lunch together. She’s leaning\nforward to stare at my upper lip.\n\n	caption: I was only half right… When I was in high school, my girlfriend at\n	the time often observed:\n\n	Girlfriend\n\n		Did you miss a spot shaving this morning?\n\n	Me\n\n		No. It just grows that way.\n\nPanel 3: I emerge from my room, drooping like a wilted flower, eyes swirly,\nbaggy, with an almost-goatee and whiskers on my face. I’m holding a book-sized\ncollection of stapled papers.\n\n	caption: My last semester in college was consumed by work on my senior\n	thesis. It was pretty much all I could think about — and still, the last\n	week or two were a frantic race to the finish. Trivial things like food,\n	sleep, or hygiene fell to the wayside."
	part-2: "Panel 4: Side view: I’m standing in front of my housemates. I’m dressed in a\nT-shirt and jeans, but I’m clean and clean-shaven again. An arrow points to my\nT-shirt labeling the “conspicuous lack of stains.” My housemates look\nunimpressed, maybe even a little confused or disturbed.\n\n	caption: When it was all over, I finally ate a hot meal with my housemates\n	again. I took a shower for the first time in what seemed like months. I took\n	a razor to my face and put on clean clothes and attempted to look like a\n	productive, well-adjusted member of society again.\n\n	Housemate 1\n\n		You look… different.\n\n	Housemate 2\n\n		You look transsexual.\n\n	Housemate 3\n\n		You look like a child-rapist. Like, not a raper of children, but a child\n		who *is* a rapist.\n\nPanel 5: I step away from them, holding up a hand to ward off the trash talk.\nHousemate 1 is still in view.\n\n	Me\n\n		Ouch, jeeze! See if I ever put in an effort again.\n\n	Housemate 1\n\n		*What* effort?\n\nPanel 6: Close-up on my frowny face, with long exaggerated spaghetti whiskers.\n\n	Title and Byline\n\n		Movember\n\n		by Niccolo Florence\n\nEND."

# Where users can download source files.
source:
	flickr: "https://www.flickr.com/photos/andrewtoskin/albums/72157661243969489"
	internet_archive: "https://archive.org/details/Movember--by-Niccolo-Florence"

# Attributions (for stories that borrow CC works, rather than collaborating with someone)
attributions:
	# Can't find the original links anymore.
	"textures by Bittbox.com":
		url: "http://www.bittbox.com/"
---

Facial hair! Also, [charity](https://no-shave.org/), or something.

(Yeah, I know, it's a little late.)
