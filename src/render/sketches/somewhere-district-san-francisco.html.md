---
title: "the Somewhere District, San Francisco"
date: "2013-08-09t13:18"
layout: "story"
series: "sketches"
license: "cc-share-alike"
blurb: "Playing with the colors here was a lot of fun. Krita, the digital painting program I used to make this piece, is crazy-awesome, and I’m still learning how to use it."

# Building the complete URL for every image.
host:
	toskin: "https://media.tosk.in/albums/andrew/sketches/2013/"
	archive: "https://ia801401.us.archive.org/28/items/sketches-2013--by-Andrew-Toskin/"
base_name: "the-somewhere-district-san-francisco--"
mediatype: ".jpg"

# Picture posts focus more on images, and may use multiple base file names.
picture_post:
	sketch_1:
		base_name: "the-somewhere-district-san-francisco"
		alt: "We see a pseudo-neoclassical building, tall, with columns and busts on its white walls, but a red clay tile roof. Opposite it, there is strange, very modern building, its trapezoidal glass reflecting the clouds and blues of the sky and the green in the foreground. The glass building has a sign that reads GOOSE.com, probably some kind of tech company. The streets are empty. Closer to us but still far away, a child leaps out of a swing seat. And right in front of us, at the bottom of the frame, is a short tree, and a police officer overlooking the scene, holding a billy club in a tight fist in a way which seems dissonant with tranquility of the rest of the image."

# Where users can download source files.
source:
	flickr: "https://www.flickr.com/photos/andrewtoskin/albums/72157647265947712"
	archive: "https://archive.org/details/sketches-2013--by-Andrew-Toskin"
---

I'm graduating soon, and I'll be away from the Bay Area for who-knows-how-long.
I just wanted to experiment with the painting software I'm learning, but I must
also have been thinking about leaving home and coming home.
