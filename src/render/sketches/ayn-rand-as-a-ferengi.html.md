---
title: "Ayn Rand as a Ferengi"
date: "2014-09-14t18:05"
layout: "story"
series: "sketches"
license: "cc-share-alike"
blurb: "Hm, let’s see. Are there any Star Trek fans here who like (making fun of) Objectivism?"

# Building the complete URL for every image.
host:
	toskin: "https://media.tosk.in/albums/andrew/sketches/2014/"
	archive: "https://ia902306.us.archive.org/26/items/Ayn-Rand-as-a-Ferengi--by-Niccolo-Florence/"
base_name: "ayn-rand-as-a-ferengi--"
mediatype: ".jpg"

# Picture posts focus more on images, and may use multiple base file names.
picture_post:
	sketch_1:
		base_name: "ayn-rand-as-a-ferengi--full"
		alt: "A picture of Ayn Rand, painted with the yellow eyes, lumpy forehead and cranium, and giant ears of a Ferengi (the epitomically greedy race of aliens in the Star Trek series).\n\nCaptions:\n\nThe Rules of Acquisition, by Ayn Rand.\n\nMoney is the barometer of a society’s virtue.\n\nHave you got the lobes to call altruism evil?\n\nGreed is eternal.\n\nA Ferengi without profit is no Ferengi at all."

# Where users can download source files.
source:
	flickr: "https://www.flickr.com/photos/andrewtoskin/sets/72157645994076517"
	archive: "https://archive.org/details/Ayn-Rand-as-a-Ferengi--by-Niccolo-Florence"

# Attributions (for stories that borrow CC works, rather than collaborating with someone)
attributions:
	"Unknown photographer (if you recognize the portrait, let me know who did it!)":
		url: "#"
		works:
			"portrait of Ayn Rand": "#"
	"Gene Roddenberry":
		url: "http://www.imdb.com/name/nm0734472/"
		works:
			"still of Quark, from Star Trek: Deep Space Nine": "http://www.imdb.com/title/tt0092455/"
---
