---
title: "Coloring In the Lines"
date: "2013-06-16t12:08"
layout: "story"
series: "sketches"
license: "cc-zero"
blurb: "Digitally painting with Krita and GIMP, as I’d recently discovered, opens a bunch of exciting new possibilities for me. These were some of my first attempts at digital painting."

# Building the complete URL for every image.
host:
	toskin: "https://media.tosk.in/albums/andrew/sketches/2013/"
	archive: "https://ia601401.us.archive.org/28/items/sketches-2013--by-Andrew-Toskin/"
# In this case, `base_name` is used only for generating the path to the thumbnail in archive lists.
base_name: "johann-heinrich-roos--landscape-with-horsemen-and-cattle--"
mediatype: ".jpg"

picture_post:
	sketch_1:
		base_name: "henri-joseph-harpignies--view-at-hérisson-allier--colored-in-krita"
		alt: "A landscape painting, showing trees growing on the rocky banks of a river\nat Hérisson, Allier, France."
		caption: "“View at Hérisson, Allier,” colored in Krita. Check out the\n<a href=\"https://commons.wikimedia.org/wiki/File:Henri-Jospeh_Harpignies_-_View_at_H%c3%a9risson,_Allier_-_Walters_37181.jpg\"\n	rel=\"cite\">\n	original version, by Henri-Joseph Harpignies\n</a>."
	sketch_2:
		base_name: "johann-heinrich-roos--landscape-with-horsemen-and-cattle--colored-with-gimp"
		alt: "A landscape painting, showing horsemen and dogs just crossing a small\nwooden bridge, on a road leading to a cathedral in the distance. Woods\nlie the left of the road, a pasture and more trees with grazing cattle\nlies on the right."
		caption: "“Landscape with Horsemen and Cattle,” colored in GIMP. Check out the\n<a href=\"https://commons.wikimedia.org/wiki/File:Johann_Heinrich_Roos_-_Landscape_with_Horsemen_and_Cattle_-_Walters_37561.jpg\"\n	rel=\"cite\">\n	original version, by Johann Heinrich Roos\n</a>."

# Where users can download source files.
source:
	flickr: "https://www.flickr.com/photos/andrewtoskin/sets/72157647265947712/"
	archive: "https://archive.org/details/sketches-2013--by-Andrew-Toskin"

# Attributions appear in the image captions in this case.
---

I just started playing with [Krita](https://krita.org/) for the first time,
and I'm trying out some new brushes for [GIMP](http://www.gimp.org/),
and I like how it’s turning out. These are still the poor scribblings of
a novice, and I'll probably be embarrassed soon enough to look back at
these, but for now I feel like hanging these up on the fridge.
