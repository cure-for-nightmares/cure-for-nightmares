---
title: "Tony has problems…"
date: "2014-09-07t16:27"
layout: "story"
series: "sketches"
license: "cc-share-alike"
blurb: "Tony has problems… A surreal portrait."

# Building the complete URL for every image.
host:
	toskin: "https://media.tosk.in/albums/andrew/sketches/2014/"
	archive: "https://ia902308.us.archive.org/11/items/Tony--by-Niccolo-Florence/"
base_name: "tony--"
mediatype: ".jpg"

# Picture posts focus more on images, and may use multiple base file names.
picture_post:
	sketch_1:
		base_name: "tony--full"
		alt: "Portrait of a man with a mangled ear and upside-down head."

# Where users can download source files.
source:
	flickr: "https://www.flickr.com/photos/andrewtoskin/sets/72157645994076517"
	archive: "https://archive.org/details/Tony--by-Niccolo-Florence"
---
