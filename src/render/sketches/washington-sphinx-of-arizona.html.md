---
title: "the Washington Sphinx of Arizona"
date: "2015-08-31t12:05"
layout: "story"
series: "sketches"
license: "cc-share-alike"
blurb: "You must answer George Washington’s riddle, sneak past the Jeffersons guarding the gates, and defeat the Benjamin Franklin wandering the labyrinth to retrieve the golden fleece."

# Building the complete URL for every image.
host:
	toskin: "https://media.tosk.in/albums/andrew/sketches/2015/"
	archive: "https://ia801503.us.archive.org/27/items/washington-sphinx-of-arizona--by-niccolo-florence/"
base_name: "washington-sphinx-of-arizona--"
mediatype: ".jpg"

# Picture posts focus more on images, and may use multiple base file names.
picture_post:
	sketch_1:
		base_name: "washington-sphinx-of-arizona"
		alt: "Among the iconic sand dunes of Giza, Egypt, between the pyramids, stands the majestic and mysterious Sphinx — topped with George Washington’s head."
		caption: ""

# Where users can download source files.
source:
	flickr: "https://www.flickr.com/photos/andrewtoskin/albums/72157655689598964"
	archive: "https://archive.org/details/washington-sphinx-of-arizona--by-niccolo-florence"

# Attributions (for stories that borrow CC works, rather than collaborating with someone)
attributions:
	"Roderick Eime":
		url: "https://www.flickr.com/photos/rodeime/"
		works:
			"Egypt Giza Sphinx with pyramids in the background": "https://www.flickr.com/photos/rodeime/8121412716/"
	"Kaleb Nyquist":
		url: "https://www.flickr.com/photos/nyquik/"
		works:
			"Geology Two - Mister George Washington on Mount Rushmore": "https://www.flickr.com/photos/nyquik/8611349115/"
---

Just got back from a long road trip around the country, visiting our
national monuments --- such as this highly underappreciated wonder in
Arizona. Frédéric Auguste Bartholdi doesn't get nearly as much attention as
he deserves!
