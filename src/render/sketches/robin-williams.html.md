---
title: "Robin Williams, 1951 – 2014"
date: "2014-08-12t18:54"
layout: "story"
series: "sketches"
license: "cc-share-alike"
blurb: "Empathy for a dead man. I hope, at this point, I’m not breaking the news to you, am I? I might have tried to ease you into it more gently."

# Building the complete URL for every image.
host:
	toskin: "https://media.tosk.in/albums/andrew/sketches/2014/"
	archive: "https://ia902307.us.archive.org/22/items/Robin-Williams-tribute--by-Niccolo-Florence/"
base_name: "robin-williams--"
mediatype: ".jpg"

# Picture posts focus more on images, and may use multiple base file names.
picture_post:
	sketch_1:
		base_name: "robin-williams--full"
		alt: "I didn’t know you. But I know how it feels, Robin."

# Where users can download source files.
source:
	flickr: "https://www.flickr.com/photos/andrewtoskin/sets/72157645994076517"
	archive: "https://archive.org/details/Robin-Williams-tribute--by-Niccolo-Florence"

# Attributions (for stories that borrow CC works, rather than collaborating with someone)
attributions:
	"director Patrick Stettner, cinematographer Lisa Rinzler, IFC Films":
		url: "http://www.imdb.com/name/nm0828099/"
		works:
			"still of Robin Williams in The Night Listener (2006 film)": "http://www.imdb.com/title/tt0448075/"
---

Like so many other people, when I heard the news, the first thing I did was
blink at the headline, read it to myself a few more times, just to make
sure I understood it correctly. "No way." Like so many other people, the
second thing I did was push all of his standup and his best movies to the
top of my watch list.

You've probably already heard? Robin Williams died by suicide yesterday.
Immediately, people started speculating: Robin hadn't realized at first how
much he'd sold his privacy, and the costs of fame took their toll on him.
He was backsliding into previously conquered territories of depression and
addiction.

I dunno, I'm still a little stunned. I'm not often this affected by the
death of celebrities. Usually I'll feel the vague twinge of empathy --- his
family must be having a hard time, trying to grieve and dodge interviewers
at the same time --- but usually I'm selfish enough that most of what I feel
is disappointment that they won't be making any more art. (Or worse, if I
wasn't a fan of theirs, I have the gall to feel nothing.)

I think what might be different in this case is that I didn't just watch a
lot of Robin Williams' movies or read a lot of his interviews. Or see the
adorable videos of him
[playing video games with his daughter Zelda](https://www.youtube.com/watch?v=wcZhY_Zo-yg "Why Robin Williams named his daughter Zelda"),
or [meeting Koko the gorilla](https://www.youtube.com/watch?v=GorgFtCqPEs).
I think what really gets me about this is the suicide. I feel
something similar every time I learn of someone who commits suicide --
because we were once comrades, even if we didn't share the same foxhole.
