---
title: "a Crown of Lights"
date: "2014-04-24t23:42"
layout: "story"
series: "photography"
license: "cc-share-alike"
blurb: "My parents have a set of portraits of Emily from when she was very young. For years, I’d thought these photos had been taken by hikaru starr. The pictures are very cute and very professional, and I’d always taken it as an example of hikaru’s skills, even back then, when he was a much less experienced photographer. Turns out some other portrait photographer shot those pictures, and I only recently learned about this fact. Later, while laughing about it with Emily, I started speculating about how, if he <em>had</em> done the pictures, hikaru probably would have taken a much more artful direction, playing more with the lighting and so on… So we, Emily and I, decided to create those pictures ourselves."

# Building the complete URL for every image.
host:
	toskin: "https://media.tosk.in/albums/andrew/photography/a-crown-of-lights/"
	archive: "https://ia802303.us.archive.org/25/items/A-Crown-of-Lights--by-Andrew-Toskin/"
base_name: "a-crown-of-lights--"
mediatype: ".jpg"

# Picture posts focus more on images, and may use multiple base file names.
picture_post:
	photo_1:
		base_name: "NIC_0595"
		alt: "Emily gazes intensely into the camera, leaning forward, face lit from below. She wears a crown of lights."
		# caption: "" # remember to convert smart punctuation here.
	photo_2:
		base_name: "NIC_0602"
		alt: "Emily leans back, still staring at us, but her face is turned slightly away. She does not look pleased."
		# caption: ""
	photo_3:
		base_name: "NIC_0615"
		alt: "Emily raises the snowflake-shaped Christmas ornament from her lap, the light full and golden on her face, now."
		# caption: ""
	photo_4:
		base_name: "NIC_0620"
		alt: "Her face turns from sleepy serenity to wonder."
		# caption: ""

# Where users can download source files.
source:
	flickr: "https://www.flickr.com/photos/andrewtoskin/sets/72157644312524473/"
	archive: "https://archive.org/details/A-Crown-of-Lights--by-Andrew-Toskin"
---

Playing with Christmas lights on a warm spring night.
