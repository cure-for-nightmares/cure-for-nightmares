---
title: "Lake Tahoe, fall 2014"
date: "2014-11-22t12:36"
layout: "story"
series: "photography"
license: "cc-share-alike"
blurb: "You might look at these and think “He obviously played around with the colors when developing these photos,” but I had to. This is actually what it looked like when I was there."

# Building the complete URL for every image.
host:
	toskin: "https://media.tosk.in/albums/andrew/photography/lake-tahoe-fall-2014/"
	archive: "https://ia801407.us.archive.org/3/items/Lake-Tahoe-fall-2014/"
# In this case, `base_name` is used only for generating the path to the thumbnail in archive lists.
base_name: "lake-tahoe-fall-2014--"
mediatype: ".jpg"

# Picture posts focus more on images, and may use multiple base file names.
picture_post:
	photo_1:
		base_name: "2014-11-10--14-20-52_01"
		alt: "A blonde girl sits by white boulders on the shore, next to the emerald\ngreen and intense blue of the Tahoe waters. Shadows grow long across the\nwaning afternoon."
	photo_2:
		base_name: "2014-11-10--14-34-25"
		alt: "A pair of boys, possibly two brothers, climb over the lakeside boulders.\nThe older of the two slides cautiously down from the top of one boulder\nto another lower boulder; the younger boy leaps from rock to rock."
	photo_3:
		base_name: "2014-11-10--14-32-17"
		alt: "We follow the same boys as they clamber further into the distance. The\ncolor fades to a sort of cyanotype monochrome."
	photo_4:
		base_name: "2014-11-10--14-46-00"
		alt: "A dizzying view straight up along the side of a tree. The trunk shoots up\nfrom the right side of the image toward the center, where branches spray\nout in every direction. Color saturation is turned down here to\nemphasize the texture of the bark."
	photo_5:
		base_name: "2014-11-10--14-51-39"
		alt: "Amid all the rich blues and greens on sunny Lake Tahoe, one tree has\nembraced the colors of autumn so vividly it almost looks like it’s on\nfire."

# Where users can download source files.
source:
	flickr: "https://www.flickr.com/photos/andrewtoskin/sets/72157649413975535/"
	archive: "https://archive.org/details/Lake-Tahoe-fall-2014"
---

Having spent most of my life in California, I was surprised to learn that
there is any kind of beach-going experience in Nevada. But Lake Tahoe, and
especially Sand Harbor, is lovely... except that it's a little too cold
for me to swim much, especially in November.
