---
title: "Marcel Duchamp"
date: "2013-10-10t23:36"
layout: "story"
series: "photography"
license: "cc-share-alike"
blurb: "I was walking home one day, thinking about how I was <em>basically</em> a brilliant polymath genius, the voice of this generation of this decade, y’know, obvi, and that I ought to have my name canonized among the great Modernists and Postmodernists — and then I saw this."

# Building the complete URL for every image.
host:
	toskin: "https://media.tosk.in/albums/andrew/photography/marcel-duchamp/"
	archive: "https://ia902502.us.archive.org/19/items/Its-a-Condo--by-Andrew-Toskin/"
base_name: "marcel-duchamp--"
mediatype: ".jpg"

# Picture posts focus more on images, and may use multiple base file names.
picture_post:
	photo:
		base_name: "marcel-duchamp"
		alt: "We see a yellow fire hydrant surrounded by dirt and dead weeds. There’s a bit of graffiti: Someone has scribbled the letters BCX near the top."

# Where users can download source files.
source:
	flickr: "https://secure.flickr.com/photos/andrewtoskin/14757640896/"
	archive: "https://archive.org/details/Its-a-Condo--by-Andrew-Toskin"
---

There. Am I a genius now?
